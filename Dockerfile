FROM openjdk:8-jre-alpine
RUN adduser -h /var/lib/simple-app -D simpleadd simpleadd
USER simpleadd
WORKDIR /var/lib/simple-app
ADD ./target/simple-app-*.jar simple-app.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "simple-app.jar"]

